import { Component, OnInit } from '@angular/core';
import { IStudent } from 'src/app/shared/model/istudent';
import { StuServiceService } from 'src/app/shared/service/stu-service.service';

@Component({
  selector: 'mytable',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  stu_list!:IStudent[]
  constructor(private _studentService:StuServiceService) { }

  ngOnInit(): void {
    this._studentService.getAllStudent().subscribe((students:IStudent[])=>{this.stu_list=students})
  }

  deleteStudent(){
    
  }
}
