import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IStudent } from 'src/app/shared/model/istudent';
import { StuServiceService } from 'src/app/shared/service/stu-service.service';

@Component({
  selector: 'myform',
  templateUrl: './myform.component.html',
  styleUrls: ['./myform.component.css']
})
export class MyformComponent implements OnInit {

  buttonName:String = "Add New"
  hideForm:boolean=false

  submitted=false

  studentForm = this.formBuilder.group({
     stuName: ['',Validators.required],
     stuGender: ['',Validators.required],
     stuSubject: ['',Validators.required]
  })
  studentList!:IStudent
  constructor(private formBuilder:FormBuilder, private _stuService:StuServiceService) { 
    
    
    
    
  }

  ngOnInit(): void {
    
   
  }

  toggleForm() {
    if(this.hideForm){
      this.hideForm = false
      this.buttonName = "Add New"
    }
    else{
      this.hideForm = true
      this.buttonName = "Hide"
    }
  }

  submitForm(){
    this.submitted = true
    if(this.studentForm.invalid){
      return
    }
    else{
      this.studentList = <IStudent>this.studentForm.value
      this._stuService.addStudent().subscribe((stulist:IStudent[])=>{
          stulist.push(this.studentList)
        
          
      })
    }
    
  }
}
