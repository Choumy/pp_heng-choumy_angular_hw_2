import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IStudent } from '../model/istudent';

@Injectable({
  providedIn: 'root'
})
export class StuServiceService {

  studentObs$=new Observable<IStudent[]>()

  students: IStudent[] = [
    {
      stuName: 'Heng Choumy',
      stuGender: 'Male',
      stuSubject: 'Spring'
    },
    {
      stuName: 'Hab Sal',
      stuGender: 'Female',
      stuSubject: 'Spring'
    },
  ]
  constructor() {
    this.studentObs$ = of(this.students)
   }

  getAllStudent():Observable<IStudent[]>{
    return this.studentObs$
  }
  addStudent():Observable<any>{
    return this.studentObs$
    console.log(this.studentObs$);
    
  }
}
